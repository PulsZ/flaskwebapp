# app/database.py

from flask_sqlalchemy import SQLAlchemy  # pyre-ignore

db = SQLAlchemy()
